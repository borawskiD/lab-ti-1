<!DOCTYPE html>
<html lang="pl">
<head>
  <meta charset="utf-8">
  <title>Hurtownia szkolna</title>
  <link href="resources/css/styl.css" rel="stylesheet" />
  <link href="https://fonts.cdnfonts.com/css/georgia" rel="stylesheet">
</head>
<body>

<div class="container">

<header class="item item-1">
<h1>Hurtownia z najlepszymi cenami</h1>
</header>



<section class="item item-2" id="leftSidebar">

<h2>Nasze ceny</h2>

<table>
<?php
$link = new mysqli("localhost", "root", "", "sklep");
if ($link->connect_error)
{
    echo "Wystąpił błąd: " . $link->connect_error;
    exit();
}

$sql = "SELECT cena, nazwa FROM towary";

$dane = $link->query($sql);
while ($rek = $dane->fetch_object())
{
    echo "<tr><td>$rek->nazwa</td><td>$rek->cena</td></tr>";
}
$link->close();
?>
</table>


</section>


<section class="item item-3" id="middlePanel">

<h2>Koszt zakupów</h2>
  <form method="post" action="index.php">
    <span class="formElement">
    <label for="selectProduct">
    Wybierz artykuł:
    </label>
    <select name="selectedProduct">


    <?php
$link = new mysqli("localhost", "root", "", "sklep");
if ($link->connect_error)
{
    echo "Wystąpił błąd: " . $link->connect_error;
    exit();
}
$sql = "SELECT nazwa FROM towary";
$dane = $link->query($sql);

while ($rek = $dane->fetch_object())
{
    echo "<option>$rek->nazwa</option>";
}
$link->close();
?>


    </select>
  </span>
  <span class="formElement">
    <label for="quantitySelector">
    Liczba sztuk:
    </label>
    <input type="number" name="quantitySelector" value="0"/> 
  </span>
      <button type="submit">OBLICZ</button>
  </form>

<!---
Zastosowałem prepared statement żeby zapobiec popsuciu przez edycję HTMLa za pomocą zbadaj element
Możliwy overkill :/
-->
<?php if (isset($_POST["selectedProduct"]) && isset($_POST["quantitySelector"]))
{
    $product_name = $_POST["selectedProduct"];
    $quantity = $_POST["quantitySelector"];
    $link = new mysqli("localhost", "root", "", "sklep");

    if ($link->connect_error)
    {
        echo "Wystąpił błąd: " . $link->connect_error;
        exit();
    }

    $sql = "SELECT cena FROM towary WHERE nazwa = ?";
    $stmt = $link->prepare($sql);
    $stmt->bind_param("s", $product_name);
    $stmt->execute();
    $result = $stmt->get_result();
    if (mysqli_num_rows($result) > 0)
    {
        $user = $result->fetch_assoc();
        $price = (float)$user["cena"] * (float)$quantity;
        echo "wartość zakupów: ".$price;
    }
    else
    {
        echo "Nie znaleziono produktu, spróbuj ponownie";
    }
} ?>


</section>

<section class="item item-4" id="rightSidebar">

  <h2>Kontakt</h2>

  <img src='resources\img\zakupy.png' alt="hurtownia">

  <br />

  <a href="mailto:hurt@poczta2.pl">email: hurt@poczta2.pl</a>


</section>
<footer class="item item-5">
  <h4>Witrynę wykonał: Dominik Borawski</h4>
</footer>
</div>
</body>
</html>
